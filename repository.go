package main

import (
	"context"
	"encoding/base64"
	"errors"
	"log"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	pb "gitlab.com/lazybasterds/alpaca/image-service/proto/image"
)

const (
	dbName = "alpacadb"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetImage(ctx context.Context, imageID string) (*pb.Image, error)
	Close(ctx context.Context)
}

//ImageRepository concrete implementation of the Repository interface.
type ImageRepository struct {
	db *DBConnection
}

// GetImage gets the information about the current building that is stored in the database
func (repo *ImageRepository) GetImage(ctx context.Context, imageID string) (*pb.Image, error) {

	objectID, err := primitive.ObjectIDFromHex(imageID)
	if err != nil {
		return nil, err
	}

	cur, err := repo.db.gridfs.Find(bson.M{
		"_id": objectID,
	})

	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer cur.Close(ctx)

	var result Image

	for cur.Next(ctx) {
		err := cur.Decode(&result)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		dls, err := repo.db.gridfs.OpenDownloadStream(objectID)
		if err != nil {
			log.Println("Failed to open download stream. ", err)
			return nil, err
		}
		defer dls.Close()

		data := make([]byte, int(result.Length))
		read, err := dls.Read(data)

		if err != nil {
			log.Println(err)
			return nil, err
		}

		if read == 0 {
			log.Println("Failed to read from stream. Bytes read = ", read)
			return nil, errors.New("Failed to read from stream")
		}

		result.Data = base64.StdEncoding.EncodeToString(data)

		// There is only one instance of the file with that particular ID
		break
	}

	uploadDate, ok := result.UploadDate.(primitive.DateTime)
	if !ok {
		uploadDate = 0
	}

	return &pb.Image{
		Id:         result.ID.Hex(),
		Length:     result.Length,
		Chunksize:  result.ChunkSize,
		Uploaddate: int64(uploadDate),
		Filename:   result.Filename,
		Data:       result.Data,
	}, nil
}

//Close closes the session
func (repo *ImageRepository) Close(ctx context.Context) {
	// TODO: Revisit if this is the correct context when closing session
	repo.db.session.EndSession(ctx)
}
