package main

import (
	"context"
	"fmt"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/image-service/proto/image"
)

type service struct {
	db *DBConnection
}

func (s *service) GetRepo() Repository {
	return &ImageRepository{s.db}
}

func (s *service) GetImage(ctx context.Context, req *pb.Image, res *pb.ImageResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()
	defer repo.Close(ctx)

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	imageID := req.Id
	image, err := repo.GetImage(ctx, imageID)
	if err != nil {
		log.Printf("Error: Failed to get image with ID %s - %+v", imageID, err)
		return err
	}
	log.Printf("Method: GetImage, RequestID: %s, Return: %s", requestID, toString(image))
	res.Image = image
	return nil
}

func (s *service) GetImages(ctx context.Context, req *pb.Image, res *pb.ImageResponse) error {
	return nil
}

func toString(i *pb.Image) string {
	return fmt.Sprintf("{Id: %v, Length: %v, Chunksize: %v, Uploaddate: %v, Filename: %v}", i.Id, i.Length, i.Chunksize, i.Uploaddate, i.Filename)
}
